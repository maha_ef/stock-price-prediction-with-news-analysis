#!/bin/bash

# update branch main 
git checkout main
git pull origin main

# get list of all existing branches
for branch in $(git branch --format='%(refname:short)' | grep -v "main")
do
  echo "Updating branch $branch" >> logs/git_log.log
  git checkout $branch
  git merge main

  # Check for merge conflicts
  if [ $? -ne 0 ]; then
    echo "Merge conflict in branch $branch." >> logs/git_log.log
    exit 1
  else
    echo "Merge for branch $branch was successful. Submitting changes...\n" >> logs/git_log.log
    git push origin $branch
  fi
done

# Return to main
git checkout main