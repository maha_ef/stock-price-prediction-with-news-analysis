from transformers import logging as transformers_logging
from transformers import AutoModel, AutoTokenizer
from database_functions.utils import DB_utils
from omegaconf import DictConfig
import pandas as pd
import numpy as np
import transformers
import logging
import hydra
import yaml


@hydra.main(config_path="configs", config_name="config", version_base='1.3.2')
def main(cfg: DictConfig):
    try:
        with open(cfg.logger.sync_logger_config_path, 'r') as file:
            logger_config = yaml.safe_load(file.read())
            logging.config.dictConfig(logger_config)
        logger = logging.getLogger('full_logger')
        logger.info("New run of experiments with BERT")
        
        # transformers_logging.set_verbosity_debug()
        # transformers_logging.enable_propagation() 
        # transformers_logging.set_verbosity(logging.DEBUG)

        db_object = DB_utils(cfg=cfg, logger=logger)
        db_object.connect(connect_to_postgres=False)
        df = db_object.get_news_within_timeframe(start_date='2020-01-01', end_date='2024-04-30')
        df = pd.DataFrame(df, columns=['publish_date', 'news'])
        model_name = "./transformers_base/DeepPavlov-rubert-base-cased" 
        tokenizer = AutoTokenizer.from_pretrained(model_name, clean_up_tokenization_spaces=True)
        model = AutoModel.from_pretrained(model_name)
        print(model)
        # token_lengths = df['news'].apply(lambda x: len(tokenizer.encode(x, add_special_tokens=True)))
        # mean_length = token_lengths.mean()
        # median_length = token_lengths.median()
        # max_length = token_lengths.max()
        # percentile_95_length = np.percentile(token_lengths, 95)

        # print(f"Mean token length: {mean_length}")
        # print(f"Median token length: {median_length}")
        # print(f"Max token length: {max_length}")
        # print(f"Top 95% percentile token length: {percentile_95_length}")


    except Exception as e:
        logger.error(str(e))

    finally:
        db_object.close_connection()
        logger.info('End of current run\n')


if __name__ == "__main__":
    main()