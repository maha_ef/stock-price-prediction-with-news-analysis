from other.utils import reduce_list, filter_data_by_dates
from typing import Dict, Optional, List, Tuple, Any
from datetime import datetime, timedelta
from omegaconf import DictConfig
from logging import Logger
from psycopg2 import sql
import pandas as pd
import psycopg2


class DB_utils():
    def __init__(
        self, 
        cfg: DictConfig,
        logger: Logger
    ):
        self._cfg = cfg
        self._logger = logger
        self._conn = None
        self._cursor = None
        self._existed_dates = {}
        self._existed_training_dates = ()


    def close_connection(self):
        if self._cursor is not None:
            self._cursor.close()
        if self._conn is not None:
            self._conn.close()

    def connect(
        self,
        connect_to_postgres: bool = False
    ):
        try:
            self.close_connection()

            if connect_to_postgres:
                dbname="postgres"
            else:
                dbname=self._cfg.db.database_connection.dbname
            self._logger.debug(f"Connecting to {dbname} database")

            self._conn = psycopg2.connect(
                host=self._cfg.db.database_connection.host,
                port=self._cfg.db.database_connection.port,
                user=self._cfg.db.database_connection.user,
                dbname=dbname,
                password=self._cfg.db.database_connection.password
            )
            self._conn.autocommit = True
            self._cursor = self._conn.cursor()
        except Exception as e:
            self._logger.error("Error occured while connecting to {dbname}:")
            self._logger.exception(e)
        
    
    def drop_table(
        self,
        table_name = None
    ):
        try:
            if table_name is None:
                self._logger.error(f"Table name wasn't specified:")
                self._logger.exception(e)

            drop_query = f"""DROP TABLE IF EXISTS {table_name}"""
            self._cursor.execute(drop_query)
            self._logger.info(f"Table {table_name} was dropped")
        except Exception as e:
            self._logger.error(f"An error occurred while dropping table {table_name}:")
            self._logger.exception(e)


    def check_or_create_db(self):
        try:
            self._cursor.execute("SELECT datname FROM pg_database;")
            databases = self._cursor.fetchall()
            if self._cfg.db.database_connection.dbname not in reduce_list(databases):
                create_db_query = sql.SQL("CREATE DATABASE {}").format(sql.Identifier(self._cfg.db.database_connection.dbname))
                self._cursor.execute(create_db_query)
                self._logger.info(f"Created database {self._cfg.db.database_connection.dbname}")
            else:
                self._logger.info(f"Database {self._cfg.db.database_connection.dbname} already exists")
        except Exception as e:
            self._logger.error(f"An error occurred while creating database {self._cfg.db.database_connection.dbname}:")
            self._logger.exception(e)
    

    def check_or_create_main_table(self):
        try:
            get_tables_query = """
                SELECT table_name
                FROM information_schema.tables
                WHERE table_schema = 'public';
            """
            self._cursor.execute(get_tables_query)
            tables = self._cursor.fetchall()

            if self._cfg.db.tables_info.main_table not in reduce_list(tables):
                create_table_query = f"""
                    CREATE TABLE IF NOT EXISTS {self._cfg.db.tables_info.main_table} (
                        id TEXT NOT NULL,
                        publish_date TIMESTAMP NOT NULL,
                        source VARCHAR(20) NOT NULL,
                        news TEXT NOT NULL
                    );
                """
                self._cursor.execute(create_table_query)
                self._logger.info(f"Created table {self._cfg.db.tables_info.main_table}")
            else:
                self._logger.info(f"Table {self._cfg.db.tables_info.main_table} already exists")
        except Exception as e:
            self._logger.error(f"An error occured while creating table {self._cfg.db.tables_info.main_table}:")
            self._logger.exception(e)


    def get_min_max_dates_from_main_table(self) -> Dict[str, Optional[datetime]]:
        try:
            query = f"""
                SELECT 
                    source,
                    MIN(publish_date) AS min_date,
                    MAX(publish_date) AS max_date
                FROM {self._cfg.db.tables_info.main_table}
                GROUP BY source
            """
            self._cursor.execute(query)
            self._existed_dates = self._cursor.fetchall()
            if len(self._existed_dates) == 0:
                self._logger.debug(f"Table {self._cfg.db.tables_info.main_table} was empty")
                self._existed_dates = {}
            else:
                self._existed_dates = {elem[0]: (elem[1], elem[2]) for elem in self._existed_dates}
            return self._existed_dates

        except Exception as e:
            self._logger.error(f"An error occured while getting min and max dates from {self._cfg.db.tables_info.main_table}:")
            self._logger.exception(e)


    def insert_into_main_table(
        self,
        data: pd.DataFrame = None
    ):
        try:
            data["publish_date"] = pd.to_datetime(data["publish_date"], format="%Y-%m-%d %H:%M:%S")
            data = data[~data["news"].isna()].reset_index(drop=True)
            data = filter_data_by_dates(data=data, date_ranges=self._existed_dates)
            data["news"] = data["news"].apply(lambda x: x.strip())

            if data.empty:
                self._logger.warning("DataFrame is empty. No data to insert.")
            
            else:
                columns = ", ".join(data.columns)
                values = ",\n\t".join(
                    self._cursor.mogrify("(%s, %s, %s, %s)", tuple(row)).decode("utf-8")
                    for row in data.itertuples(index=False, name=None)
                )
                insert_query = f"INSERT INTO {self._cfg.db.tables_info.main_table} ({columns}) VALUES\n\t{values};"
                self._cursor.execute(insert_query)
                self._logger.info(f"{data.shape[0]} rows were inserted into table {self._cfg.db.tables_info.main_table}")
        except Exception as e:
            self._logger.error(f"An error occured while inserting data into {self._cfg.db.tables_info.main_table}:")
            self._logger.exception(e)
        

    def check_or_create_training_table(self):
        try:
            training_table_name = self._cfg.stocks.training_table
            get_tables_query = """
                SELECT table_name
                FROM information_schema.tables
                WHERE table_schema = 'public';
            """
            self._cursor.execute(get_tables_query)
            tables = self._cursor.fetchall()

            if training_table_name not in reduce_list(tables):
                create_table_query = f"""
                    CREATE TABLE IF NOT EXISTS {training_table_name} (
                        report_date DATE NOT NULL,
                        news TEXT NOT NULL,
                        price_change NUMERIC(10, 8) NOT NULL
                    );
                """
                self._cursor.execute(create_table_query)
                self._logger.info(f"Created table {training_table_name}")
            else:
                self._logger.info(f"Table {training_table_name} already exists")
        
        except Exception as e:
            self._logger.error(f"An error occured while creating table {training_table_name}:")
            self._logger.exception(e)

    
    def get_min_max_dates_from_stocks_table(self) -> Optional[Tuple[datetime, datetime]]:
        try:
            training_table_name = self._cfg.stocks.training_table
            query = f"""
                SELECT 
                    MIN(report_date) AS min_date,
                    MAX(report_date) AS max_date
                FROM {training_table_name}
            """
            self._cursor.execute(query)
            self._existed_training_dates = self._cursor.fetchall()[0]
            if self._existed_training_dates[0] is None:
                self._logger.debug(f"Table {training_table_name} was empty")
                self._existed_training_dates = ()
            return self._existed_training_dates

        except Exception as e:
            self._logger.error(f"An error occured while getting min and max dates from {training_table_name}:")
            self._logger.exception(e)
        

    def get_news_within_timeframe(
        self,
        start_date: str,
        end_date: str
    ) -> List[Tuple[datetime, str]]:
        try:
            start_date = datetime.strptime(start_date, "%Y-%m-%d")
            end_date = datetime.strptime(end_date, "%Y-%m-%d")

            start_datetime = (start_date - timedelta(days=1)).replace(hour=10, minute=30)
            end_datetime = end_date.replace(hour=10, minute=30)
        
            select_query = f"""
                SELECT 
                    publish_date,
                    news
                FROM {self._cfg.db.tables_info.main_table}
                WHERE publish_date >= %s AND publish_date < %s
                ORDER BY publish_date;
            """
            self._cursor.execute(select_query, (start_datetime, end_datetime))
            data = self._cursor.fetchall()
            self._logger.debug(f"{len(data)} news were fetched")
            return data
        
        except Exception as e:
            self._logger.error(f"An error occured while fetching news from timeframe:")
            self._logger.exception(e)
        

    def select_from_training_table(
        self,
        selected_date: str
    ) -> Tuple[datetime, str, float]:
        try:
            training_table_name = self._cfg.stocks.training_table
            select_query = f"""
                SELECT 
                    report_date,
                    news,
                    price_change
                FROM {training_table_name}
                WHERE report_date = %s
            """
            self._cursor.execute(select_query, (selected_date, ))
            data = self._cursor.fetchall()[0]
            self._logger.debug(f"Data on {selected_date} was fetched")
            return data
        except Exception as e:
            self._logger.error(f"Error occured while fetching data from {training_table_name} on {selected_date}:")
            self._logger.exception(e)
        

    def delete_row_by_date_from_training(
        self,
        date_to_drop: str
    ):
        try:
            training_table_name = self._cfg.stocks.training_table
            delete_query = f"""
                DELETE 
                FROM {training_table_name}
                WHERE report_date = %s
            """
            self._cursor.execute(delete_query, (date_to_drop, ))
            self._logger.debug(f"Data on {date_to_drop} was deleted")
        except Exception as e:
            self._logger.error(f"An error occured while deleting row from {training_table_name}:")
            self._logger.exception(e)
    
    
    def insert_into_training_table(
        self,
        data: pd.DataFrame = None
    ):
        try:
            training_table_name = self._cfg.stocks.training_table
            data["report_date"] = pd.to_datetime(data["report_date"], format="%Y-%m-%d")
            if len(self._existed_training_dates) != 0:
                
                min_new_date = data["report_date"].min()
                max_new_date = data["report_date"].max()
                if max_new_date.date() == self._existed_training_dates[0]:
                    self.delete_row_by_date_from_training(date_to_drop=max_new_date)
                elif min_new_date.date() == self._existed_training_dates[1]:
                    data = data[data["report_date"] != min_new_date].reset_index(drop=True)

            if data.empty:
                self._logger.warning("DataFrame is empty. No data to insert.")
            
            else:
                columns = ", ".join(data.columns)
                values = ",\n\t".join(
                    self._cursor.mogrify("(%s, %s, %s)", tuple(row)).decode("utf-8")
                    for row in data.itertuples(index=False, name=None)
                )
                insert_query = f"INSERT INTO {training_table_name} ({columns}) VALUES\n\t{values};"
                self._cursor.execute(insert_query)
                self._logger.info(f"{data.shape[0]} rows were inserted into table {training_table_name}")
        except Exception as e:
            self._logger.error(f"An error occured while inserting data into {training_table_name}:")
            self._logger.exception(e)


    def get_training_dataset(self) -> List[Tuple[datetime, str, float]]:
        try:
            training_table_name = self._cfg.stocks.training_table
            select_query = f"""
                SELECT *
                FROM {training_table_name}
                ORDER BY report_date
            """
            self._cursor.execute(select_query)
            data = self._cursor.fetchall()
            self._logger.debug(f"Training data was fetched, {len(data)} rows")
            return data
        
        except Exception as e:
            self._logger.error(f"An error occured while getting training dataset:")
            self._logger.exception(e)