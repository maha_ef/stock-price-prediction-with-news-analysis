from other.utils import filter_data_by_dates
from aiologger import Logger
from typing import Dict
import pandas as pd
import asyncpg


class async_DB_utils():
    def __init__(
        self,
        cfg: Dict,
        logger: Logger
    ):
        self._cfg = cfg
        self._logger = logger
        self._conn = None
        self._existed_dates = {}

    
    async def close_connection(self):
        if self._conn is not None:
            await self._conn.close()
        

    async def connect(
        self,
        connect_to_postgres: bool = False
    ):
        try:
            await self.close_connection()

            if connect_to_postgres:
                dbname="postgres"
            else:
                dbname=self._cfg["database_connection"]["dbname"]
            await self._logger.debug(f"Connecting to {dbname} database")

            self._conn = await asyncpg.connect(
                host=self._cfg["database_connection"]["host"],
                port=self._cfg["database_connection"]["port"],
                user=self._cfg["database_connection"]["user"],
                database=dbname,
                password=self._cfg["database_connection"]["password"]
            )
        except Exception as e:
            self._logger.error(f"Error occured while connecting to {dbname}:")
            self._logger.exception(e)

        
    async def drop_table(
        self,
        table_name: str = None
    ):
        try:
            if table_name is None:
                self._logger.error(f"Table name wasn't specified:")
                self._logger.exception(e)

            drop_query = f"""DROP TABLE IF EXISTS {table_name}"""
            await self._conn.execute(drop_query)
            await self._logger.info(f"Table {table_name} was dropped")
        except Exception as e:
            self._logger.error(f"An error occurred while dropping table {table_name}:")
            self._logger.exception(e)


    async def check_or_create_db(self):
        try:
            databases = await self._conn.fetch("SELECT datname FROM pg_database;")
            databases = [record["datname"] for record in databases]
            if self._cfg["database_connection"]["dbname"] not in databases:
                await self._conn.execute(f"CREATE DATABASE {self._cfg['database_connection']['dbname']}")
                await self._logger.info(f"Created DataBase {self._cfg['database_connection']['dbname']}")
            else:
                await self._logger.info(f"Database {self._cfg['database_connection']['dbname']} already exists")
        except Exception as e:
            self._logger.error(f"An error occurred while creating database {self._cfg['database_connection']['dbname']}:")
            self._logger.exception(e)

    
    async def check_or_create_main_table(self) -> str:
        try:
            get_tables_query = """
                SELECT table_name
                FROM information_schema.tables
                WHERE table_schema = 'public';
            """
            tables = await self._conn.fetch(get_tables_query)
            tables = [record["table_name"] for record in tables]

            if self._cfg["tables_info"]["main_table"] not in tables:
                create_table_query = f"""
                    CREATE TABLE IF NOT EXISTS {self._cfg['tables_info']['main_table']} (
                        id TEXT NOT NULL,
                        publish_date TIMESTAMP NOT NULL,
                        source VARCHAR(20) NOT NULL,
                        news TEXT NOT NULL
                    );
                """
                await self._conn.execute(create_table_query)
                await self._logger.info(f"Created table {self._cfg['tables_info']['main_table']}")
            else:
                await self._logger.info(f"Table {self._cfg['tables_info']['main_table']} already exists")
        except Exception as e:
            self._logger.error(f"An error occurred while creating table {self._cfg['tables_info']['main_table']}:")
            self._logger.exception(e)


    async def get_min_max_dates_from_main_table(self):
        try:
            query = f"""
                SELECT 
                    source,
                    MIN(publish_date) AS min_date,
                    MAX(publish_date) AS max_date
                FROM {self._cfg['tables_info']['main_table']}
                GROUP BY source
            """
            self._existed_dates = await self._conn.fetch(query)
            self._existed_dates = {elem["source"]: (elem["min_date"], elem["max_date"]) for elem in self._existed_dates}
            if len(self._existed_dates) == 0:
                await self._logger.info(f"Table {self._cfg['tables_info']['main_table']} was empty")
            else:
                for source, time_periods in self._existed_dates.items():
                    self._logger.info(f"\tFor source {source} time news exist in periods from {time_periods[0]} to {time_periods[1]}")
            return self._existed_dates        
        except Exception as e:
            self._logger.error(f"An error occured while getting min and max dates:")
            self._logger.exception(e)


    async def insert_into_main_table(
        self,
        data: pd.DataFrame = None
    ) -> str:
        try:
            data["publish_date"] = pd.to_datetime(data["publish_date"], format="%Y-%m-%d %H:%M:%S")
            data = data[~data["news"].isna()].reset_index(drop=True)
            data = filter_data_by_dates(data=data, date_ranges=self._existed_dates)
            data["news"] = data["news"].apply(lambda x: x.strip())
            
            if data.empty:
                await self._logger.warning("DataFrame is empty. No data to insert.")
                return "Done"
            
            else:
                columns = ", ".join(data.columns)
                values = [
                    tuple(row)
                    for row in data.itertuples(index=False, name=None)
                ]

                insert_query = f"""
                INSERT INTO {self._cfg['tables_info']['main_table']} 
                ({columns}) VALUES ($1, $2, $3, $4)
                """

                async with self._conn.transaction():
                    await self._conn.executemany(insert_query, values)

                await self._logger.info(f"{data.shape[0]} rows were inserted into table {self._cfg['tables_info']['main_table']}")
        except Exception as e:
            self._logger.error(f"An error occured while inserting data into {self._cfg['tables_info']['main_table']}:")
            self._logger.exception(e)    

