from database_functions.async_utils import async_DB_utils
from parsers.rbc_parser_async import rbc_parser_async
from other.utils import get_unique_date_intervals
from async_logger.utils import setup_logger
import asyncio
import yaml


async def main():
    try:
        logger = await setup_logger(["console", "file"])
        await logger.info("New run of data parsing")

        with open("./configs/rbc/rbc_config.yaml", "r") as file:
            rbc_parser_cfg = yaml.safe_load(file.read())
        with open("./configs/experiment/main_params.yaml", "r") as file:
            experiment_params_cfg = yaml.safe_load(file.read())
        with open("./configs/db/local_psql.yaml", "r") as file:
            db_cfg = yaml.safe_load(file.read())

        rbc_parser_cfg["request_info"]["dateFrom"] = experiment_params_cfg["dateFrom"]
        rbc_parser_cfg["request_info"]["dateTo"] = experiment_params_cfg["dateTo"]

        db_object = async_DB_utils(cfg=db_cfg, logger=logger)
        # Connect to postgres and check if main DB exists
        await db_object.connect(connect_to_postgres=True)
        await db_object.check_or_create_db()

        # Connect to main db and check main news table for dates
        await db_object.connect(connect_to_postgres=False)
        await db_object.check_or_create_main_table()
        existed_dates = await db_object.get_min_max_dates_from_main_table()

        # TBD: Add iteration throgh all data sources 
        unique_date_intervals = {}
        if len(existed_dates) == 0:
            unique_date_intervals["РБК"] = [
                [
                    rbc_parser_cfg["request_info"]["dateFrom"], 
                    rbc_parser_cfg["request_info"]["dateTo"]
                ]
            ]
        else:
            unique_date_intervals["РБК"] = get_unique_date_intervals(
                new_date_left=rbc_parser_cfg["request_info"]["dateFrom"],
                new_date_right=rbc_parser_cfg["request_info"]["dateTo"],
                existed_date_left=existed_dates["РБК"][0],
                existed_date_right=existed_dates["РБК"][1]
            )
        await logger.debug(f"unique_date_intervals: {unique_date_intervals}")

        for date_from, date_to in unique_date_intervals["РБК"]:
            logger.info(f"Date intervals in work: {date_from}, {date_to}")
            rbc_parser_cfg["request_info"]["dateFrom"] = date_from
            rbc_parser_cfg["request_info"]["dateTo"] = date_to
            rbc_parser_obj = rbc_parser_async(
                cfg=rbc_parser_cfg,
                logger=logger
            )
            new_data = await rbc_parser_obj.get_articles()
            await db_object.insert_into_main_table(new_data)

    except Exception as e:
        await logger.error(str(e))

    finally:
        await db_object.close_connection()
        await logger.info("End of current run\n")
        await logger.shutdown()

# run the main function
if __name__ == "__main__":
    asyncio.run(main())