from other.utils import adjust_dates, assign_date_group, get_unique_date_intervals, group_weekends
from parsers.moex_parser import MoexParser
from database_functions.utils import DB_utils
from datetime import datetime, timedelta
from omegaconf import DictConfig
import pandas as pd
import logging
import hydra
import yaml


@hydra.main(config_path="configs", config_name="config", version_base="1.3.2")
def main(cfg: DictConfig):
    try:
        with open(cfg.logger.sync_logger_config_path, "r") as file:
            logger_config = yaml.safe_load(file.read())
            logging.config.dictConfig(logger_config)
        logger = logging.getLogger("full_logger")
        logger.info("New run of filling table for model")

        db_object = DB_utils(cfg=cfg, logger=logger)
        db_object.connect(connect_to_postgres=False)
        db_object.drop_table(table_name=cfg.stocks.training_table)
        db_object.check_or_create_training_table()
        existed_dates = db_object.get_min_max_dates_from_main_table()

        if len(existed_dates) == 0:
            logger.warning(f"No news exist in {cfg.db.tables_info.main_table}.")
        else:
            # TBD: Add check for same dates for each source
            cfg = adjust_dates(
                existed_dates=existed_dates, 
                cfg=cfg.copy(), 
                logger=logger
            )
            existed_training_dates = db_object.get_min_max_dates_from_stocks_table()
            unique_date_intervals = []
            if len(existed_training_dates) == 0:
                unique_date_intervals.append([cfg.experiment.dateFrom, cfg.experiment.dateTo])
            else:
                unique_date_intervals = get_unique_date_intervals(
                    new_date_left=cfg.experiment.dateFrom,
                    new_date_right=cfg.experiment.dateTo,
                    existed_date_left=existed_training_dates[0],
                    existed_date_right=existed_training_dates[1]
                )
            
            for left_border, right_border in unique_date_intervals:
                cfg.experiment.dateFrom = left_border
                cfg.experiment.dateTo = right_border

                moex_object = MoexParser(
                    cfg=cfg, 
                    logger=logger
                )
                prices = moex_object.parse_data()
                result = db_object.get_news_within_timeframe(
                    start_date=cfg.experiment.dateFrom,
                    end_date=cfg.experiment.dateTo
                )
                result = pd.DataFrame(result, columns=["publish_date", "news"])
                result["group_date"] = result["publish_date"].apply(assign_date_group)
                result = result\
                    .groupby("group_date")\
                        .agg({"news": " ".join})\
                            .reset_index()\
                                .rename(columns={"group_date": "publish_date"})
                result = pd.merge(
                    left=result,
                    right=prices,
                    how="inner",
                    left_on="publish_date",
                    right_on="report_date"
                )
                result = result.drop(columns=["publish_date"])
                result = group_weekends(
                    df=result, 
                    news_aggregation_type=cfg.experiment.news_aggregation_type
                )
                # print(result.head())
                db_object.insert_into_training_table(data=result)

    except Exception as e:
        logger.error(str(e))

    finally:
        db_object.close_connection()
        logger.info("End of current run\n")


if __name__ == "__main__":
    main()