# Step 0: Set environment
1.  Set repo  <input type="checkbox" disabled checked />
2.  Create venv <input type="checkbox" disabled checked />  
3.  Fill requirements.txt <input type="checkbox" disabled checked />  


# Step 1: Parse and load to database *(sync and async)*
1.   DB functions:  
    1.   Check if exists or create database <input type="checkbox" disabled checked />  
    2.   Check if exists or create table <input type="checkbox" disabled checked />  
    3.   Drop table <input type="checkbox" disabled checked />  
    4.   Get min and max dates by each data source <input type="checkbox" disabled checked />   
    5.   Insert new data <input type="checkbox" disabled checked />  
2.   RBC parser:  
    1.   Request news by general url <input type="checkbox" disabled checked />  
    2.   Request news articles by url <input type="checkbox" disabled checked />  
    3.   Load text from news articles <input type="checkbox" disabled checked />  
    4.   Return dataframe <input type="checkbox" disabled checked />  
3. Merge parser and database:
    1. Load configs <input type="checkbox" disabled checked />
    2. Create logger <input type="checkbox" disabled checked />
    3. Create database <input type="checkbox" disabled checked />
    4. Create main table <input type="checkbox" disabled checked />
    5. Parse data from RBC for missing time periods <input type="checkbox" disabled checked />
    6. Load data into the database <input type="checkbox" disabled checked />
4. Add asynchrony
    1. Asynchronous parsing RBC news <input type="checkbox" disabled checked/>
    2. Asynchronous load to database <input type="checkbox" disabled checked/>

# Step 2: Connect news and price changes
1. Parse prices:
    1. Check if exists or create table <input type="checkbox" disabled checked /> 
    2. Create parser for MOEX <input type="checkbox" disabled checked /> 
    3. Get price change as $\frac{Price_{open}-Price_{close}}{Price_{open}}$ <input type="checkbox" disabled checked /> 
2. Merge news and prices tables
    1. Merge tables on date <input type="checkbox" disabled checked /> 
    2. Concat news with zero price change to next first date with non-zero price_change <input type="checkbox" disabled checked /> 
    3. Load table <input type="checkbox" disabled checked /> 
