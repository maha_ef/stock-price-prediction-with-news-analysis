from other.utils import get_unique_date_intervals
from database_functions.utils import DB_utils
from omegaconf import DictConfig, OmegaConf
from parsers.rbc_parser import rbc_parser
import logging
import hydra
import yaml


@hydra.main(config_path="configs", config_name="config", version_base="1.3.2")
def main(cfg: DictConfig):
    # print(OmegaConf.to_yaml(cfg))

    try:
        with open(cfg.logger.sync_logger_config_path, "r") as file:
            logger_config = yaml.safe_load(file.read())
            logging.config.dictConfig(logger_config)
        logger = logging.getLogger("full_logger")
        logger.info("New run of data parsing")
        
        db_object = DB_utils(cfg=cfg, logger=logger)
        # Connect to postgres and check if main DB exists
        db_object.connect(connect_to_postgres=True)
        db_object.check_or_create_db()

        # Connect to main db and check main news table for dates
        db_object.connect(connect_to_postgres=False)
        db_object.check_or_create_main_table()
        existed_dates = db_object.get_min_max_dates_from_main_table()

        # TBD: Add iteration throgh all data sources 
        unique_date_intervals = {}
        if len(existed_dates) == 0:
            unique_date_intervals["РБК"] = [[cfg.experiment.dateFrom, cfg.experiment.dateTo]]
        else:
            unique_date_intervals["РБК"] = get_unique_date_intervals(
                new_date_left=cfg.experiment.dateFrom,
                new_date_right=cfg.experiment.dateTo,
                existed_date_left=existed_dates["РБК"][0],
                existed_date_right=existed_dates["РБК"][1]
            )

        for date_from, date_to in unique_date_intervals["РБК"]:
            logger.info(f"Date intervals in work: {date_from}, {date_to}")
            cfg.experiment.dateFrom = date_from
            cfg.experiment.dateTo = date_to
            rbc_parser_obj = rbc_parser(
                cfg=cfg,
                logger=logger
            )
            new_data = rbc_parser_obj.get_articles()
            db_object.insert_into_main_table(new_data)

    except Exception as e:
        logger.error(str(e))

    finally:
        db_object.close_connection()
        logger.info("End of current run\n")


if __name__ == "__main__":
    main()