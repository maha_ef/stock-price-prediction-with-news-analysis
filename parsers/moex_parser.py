from omegaconf import DictConfig
from logging import Logger
from typing import Dict
import datetime
from time import sleep
import pandas as pd
import requests


class MoexParser():
    def __init__(
        self, 
        cfg: DictConfig,
        logger: Logger
    ) -> None:
        self._cfg = cfg
        self._logger = logger
        self._url = f"https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR/securities/{self._cfg.stocks.moex_name}/candles.json"
    

    def _fetch_data(self) -> Dict:
        results = None
        dateFrom= self._cfg.experiment.dateFrom
        delta = (datetime.datetime.strptime(self._cfg.experiment.dateTo, "%Y-%m-%d") - datetime.datetime.strptime(dateFrom, "%Y-%m-%d")).days
        while delta > 0:
            dateTo = datetime.datetime.strptime(dateFrom, "%Y-%m-%d") + datetime.timedelta(days=min(delta, 500))
            delta -= min(delta, 500)
            dateTo = datetime.datetime.strftime(dateTo, "%Y-%m-%d")
            
            params = {
                "from": dateFrom,
                "till": dateTo,
                "interval": 24
            }
            response = requests.get(
                self._url,
                params=params
            )

            if response.status_code == 200:
                if results is None:
                    results = response.json()
                else:
                    results["candles"]["data"].extend(response.json()["candles"]["data"])
            else:
                self._logger.error(f"An error occured while fetching stock prices: \n\t{response.status}")
            sleep(5)
        return results
    
    def parse_data(self) -> pd.DataFrame:
        try:
            self._logger.info(f"Parsing prices from {self._cfg.experiment.dateFrom} till {self._cfg.experiment.dateTo}")
            data = self._fetch_data()
            
            candles = pd.DataFrame(data["candles"]["data"], columns=data["candles"]["columns"])
            candles = candles[["begin", "open", "close", "volume"]]
            candles["price_change"] = candles.apply(
                lambda row: 100 * (row["open"] - row["close"]) / row["open"] if row["volume"] != 0 else 0, axis=1
            )
            candles = candles.rename(columns={"begin": "report_date"})
            candles["report_date"] = pd.to_datetime(candles["report_date"])

            result_table = pd.date_range(
                start=self._cfg.experiment.dateFrom, 
                end=self._cfg.experiment.dateTo, 
                freq="D"
            )

            result_table = pd.DataFrame({"report_date": result_table, "price_change": 0})
            
            result_table = pd.merge(
                result_table, 
                candles[["report_date", "price_change"]], 
                on="report_date", 
                how="left", 
                suffixes=("_df1", "_df2")
            )

            result_table["price_change"] = result_table["price_change_df2"]\
                .combine_first(result_table["price_change_df1"])

            result_table = result_table.drop_duplicates(keep="last")
            self._logger.info("Parsing prices was finished")
            return result_table[["report_date", "price_change"]]
        
        except Exception as e:
            self._logger.error(f"An error occured while parsing stocks prices:")
            self._logger.exception(e)