from omegaconf import DictConfig, open_dict
from datetime import datetime, timedelta
from bs4 import BeautifulSoup as bs
from logging import Logger
import requests as rq
import pandas as pd
import unicodedata

class rbc_parser:
    def __init__(
        self, 
        cfg: DictConfig,
        logger: Logger
    ):
        super().__init__()
        self._cfg = cfg
        self._columns_to_save = ["id", "project", "publish_date", "category", "fronturl"]
        self._cfg.experiment.dateFrom = datetime.strptime(self._cfg.experiment.dateFrom, "%Y-%m-%d").strftime("%d.%m.%Y")
        self._cfg.experiment.dateTo = datetime.strptime(self._cfg.experiment.dateTo, "%Y-%m-%d").strftime("%d.%m.%Y")
        self._logger = logger

    def _get_url(
        self, 
        param_dict: DictConfig
    ) -> str:
        """
        Возвращает URL для запроса json таблицы со статьями
        """
        url = param_dict["url"] +\
            f"project={param_dict['project']}&" +\
            f"dateFrom={param_dict['dateFrom']}&" +\
            f"dateTo={param_dict['dateTo']}&" +\
            f"page={param_dict['page']}&" +\
            f"query={param_dict['query']}"
        return url
    
    def _get_search_table(
        self, 
        param_dict: DictConfig
    ) -> pd.DataFrame:
        """
        Возвращает pd.DataFrame со списком статей
        """
        url = self._get_url(param_dict)
        r = rq.get(url)
        search_table = pd.DataFrame(r.json()["items"])
        if not search_table.empty:
            search_table = search_table[search_table["category"].isin(["Финансы", "Политика", "Бизнес", "Экономика"])]
        if not search_table.empty:
            search_table = search_table[self._columns_to_save]
            get_text = lambda x: self._get_article_data(x["fronturl"])
            search_table["news"] = search_table.apply(get_text, axis=1).tolist()
            return search_table[["id", "project", "publish_date", "news"]]
        else:
            return pd.DataFrame(columns=["id", "project", "publish_date", "news"])
    
    def _iterable_load_by_page(
        self, 
        param_dict: DictConfig
    ) -> pd.DataFrame:
        param_copy = param_dict.copy()
        results = []
        result = self._get_search_table(param_copy)
        results.append(result)
        while not result.empty:
            param_copy["page"] = str(int(param_copy["page"]) + 1)
            result = self._get_search_table(param_copy)
            results.append(result)
        results = pd.concat(results, axis=0, ignore_index=True)
        return results
    
    def _get_article_data(
        self, 
        url: str
    ) -> str:
        """
        Возвращает описание и текст статьи по ссылке
        """
        r = rq.get(url)
        soup = bs(r.text, features="lxml")
        p_text = soup.find_all("p")
        if p_text:
            text = list(
                map(
                    lambda x: \
                        x.text.replace("<br />", "\n").strip() \
                            if "\n\n\n" not in x.text else "",
                    p_text
                )
            )
            text = unicodedata.normalize("NFKD", " ".join(text[5:-11])).replace("\xa0", " ").strip()
        else:
            text = None
        return text 
    
    def get_articles(self) -> pd.DataFrame:
        """
        Функция для скачивания статей интервалами через каждые time_step дней
        Делает сохранение таблицы через каждые save_every * time_step дней
        """
        out_df = pd.DataFrame(columns=["id", "project", "publish_date", "news"])
        param_copy = self._cfg.rbc.request_info.copy()
        with open_dict(param_copy):
            param_copy["dateFrom"] = self._cfg.experiment.dateFrom
            param_copy["dateTo"] = self._cfg.experiment.dateTo
        time_step = timedelta(days=self._cfg.rbc.other.time_step)
        dateFrom = datetime.strptime(param_copy["dateFrom"], "%d.%m.%Y")
        dateTo = datetime.strptime(param_copy["dateTo"], "%d.%m.%Y")

        if dateFrom > dateTo:
            raise ValueError("dateFrom should be less than dateTo")

        while dateFrom <= dateTo:
            param_copy["dateTo"] = (dateFrom + time_step).strftime("%d.%m.%Y")
            if dateFrom + time_step > dateTo:
                param_copy["dateTo"] = dateTo.strftime("%d.%m.%Y")
            self._logger.debug("Parsing articles from " + param_copy["dateFrom"] +  " to " + param_copy["dateTo"])
            new_df = self._iterable_load_by_page(param_copy)
            out_df = pd.concat([out_df, new_df], axis=0, ignore_index=True)
            dateFrom += time_step + timedelta(days=1)
            param_copy["dateFrom"] = dateFrom.strftime("%d.%m.%Y")
        
        out_df = out_df.rename(columns={"project": "source"})
        out_df = out_df[["id", "publish_date", "source", "news"]]
        out_df["publish_date"] = pd.to_datetime(out_df["publish_date"])
        out_df["publish_date"] = out_df["publish_date"].dt.tz_localize(None)

        self._logger.info(f"Downloading data from {self._cfg.experiment.dateFrom} to {self._cfg.experiment.dateTo} was finished")
        return out_df