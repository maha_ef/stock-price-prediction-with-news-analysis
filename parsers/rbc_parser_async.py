from tenacity import retry, stop_after_attempt, wait_fixed, RetryError
from datetime import datetime, timedelta
from bs4 import BeautifulSoup as bs
from omegaconf import DictConfig
from aiologger import Logger
from typing import Dict
import pandas as pd
import unicodedata
import aiohttp
import asyncio
import json


class rbc_parser_async:
    def __init__(
        self, 
        cfg: Dict,
        logger: Logger
    ):
        super().__init__()
        self._cfg = cfg
        self._columns_to_save = ["id", "project", "publish_date", "category", "fronturl"]
        self._cfg["request_info"]["dateFrom"] = datetime.strptime(str(self._cfg["request_info"]["dateFrom"]), "%Y-%m-%d").strftime("%d.%m.%Y")
        self._cfg["request_info"]["dateTo"] = datetime.strptime(str(self._cfg["request_info"]["dateTo"]), "%Y-%m-%d").strftime("%d.%m.%Y")
        self._logger = logger

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(5), retry_error_callback=lambda _: None)
    async def _fetch_with_retry(
        self, 
        session: aiohttp.ClientSession, 
        url: str
    ) -> str:
        async with session.get(url) as response:
            response.raise_for_status()
            return await response.text()
        
    def _get_url(
        self, 
        param_dict: Dict
    ) -> str:
        """
        Возвращает URL для запроса json таблицы со статьями
        """
        url = param_dict["url"] +\
            f"project={param_dict['project']}&" +\
            f"dateFrom={param_dict['dateFrom']}&" +\
            f"dateTo={param_dict['dateTo']}&" +\
            f"page={param_dict['page']}&" +\
            f"query={param_dict['query']}"
        return url
    
    async def _get_search_table(
        self, 
        session: aiohttp.ClientSession,
        param_dict: Dict
    ) -> pd.DataFrame:
        """
        Возвращает pd.DataFrame со списком статей
        """
        url = self._get_url(param_dict)

        try:
            data = await self._fetch_with_retry(session, url)
        except RetryError:
            await self._logger.error(f"Failed to fetch data from {url} after multiple attempts.")
            return pd.DataFrame(columns=["id", "project", "publish_date", "news"])
        
        data = json.loads(data)
        search_table = pd.DataFrame(data["items"])

        if not search_table.empty:
            search_table = search_table[search_table["category"].isin(["Финансы", "Политика", "Бизнес", "Экономика"])]
        if not search_table.empty:
            search_table = search_table[self._columns_to_save]
            tasks = [self._get_article_data(session, row["fronturl"]) for _, row in search_table.iterrows()]
            search_table["news"] = await asyncio.gather(*tasks)
            return search_table[["id", "project", "publish_date", "news"]]
        else:
            return pd.DataFrame(columns=["id", "project", "publish_date", "news"])
    
    async def _iterable_load_by_page(
        self, 
        session: aiohttp.ClientSession,
        param_dict: Dict
    ) -> pd.DataFrame:
        param_copy = param_dict.copy()
        results = []
        result = await self._get_search_table(session, param_copy)
        results.append(result)
        while not result.empty:
            param_copy["page"] = str(int(param_copy["page"]) + 1)
            result = await self._get_search_table(session, param_copy)
            results.append(result)
        results = pd.concat(results, axis=0, ignore_index=True)
        return results
    
    async def _get_article_data(
        self,
        session: aiohttp.ClientSession,
        url: str
    ) -> str:
        """
        Возвращает описание и текст статьи по ссылке
        """
        try:
            data = await self._fetch_with_retry(session, url)
        except RetryError:
            await self._logger.error(f"Failed to fetch article data from {url} after multiple attempts.")
            return None

        soup = bs(data, features="lxml")
        p_text = soup.find_all("p")
        if p_text:
            text = list(
                map(
                    lambda x: \
                        x.text.replace("<br />", "\n").strip() \
                            if "\n\n\n" not in x.text \
                                else "",
                    p_text
                )
            )
            text = unicodedata.normalize("NFKD", " ".join(text[5:-11])).replace("\xa0", " ").strip()
        else:
            text = None
        return text 
    
    async def get_articles(self) -> pd.DataFrame:
        """
        Функция для скачивания статей интервалами через каждые time_step дней
        """
        out_df = pd.DataFrame(columns=["id", "project", "publish_date", "news"])
        param_copy = self._cfg["request_info"].copy()
        time_step = timedelta(days=self._cfg["other"]["time_step"])
        dateFrom = datetime.strptime(param_copy["dateFrom"], "%d.%m.%Y")
        dateTo = datetime.strptime(param_copy["dateTo"], "%d.%m.%Y")

        if dateFrom > dateTo:
            raise ValueError("dateFrom should be less than dateTo")

        async with aiohttp.ClientSession() as session:
            while dateFrom <= dateTo:
                param_copy["dateTo"] = (dateFrom + time_step).strftime("%d.%m.%Y")
                if dateFrom + time_step > dateTo:
                    param_copy["dateTo"] = dateTo.strftime("%d.%m.%Y")
                await self._logger.debug("Parsing articles from " + param_copy["dateFrom"] +  " to " + param_copy["dateTo"])
                new_df = await self._iterable_load_by_page(session, param_copy)
                out_df = pd.concat([out_df, new_df], axis=0, ignore_index=True)
                dateFrom += time_step + timedelta(days=1)
                param_copy["dateFrom"] = dateFrom.strftime("%d.%m.%Y")
        
        out_df = out_df.rename(columns={"project": "source"})
        out_df = out_df[["id", "publish_date", "source", "news"]]
        out_df["publish_date"] = pd.to_datetime(out_df["publish_date"])
        out_df["publish_date"] = out_df["publish_date"].dt.tz_localize(None)

        await self._logger.info(
            f"Downloading data from {self._cfg['request_info']['dateFrom']} to {self._cfg['request_info']['dateTo']} was finished"
        )
        return out_df