from aiologger.handlers.streams import AsyncStreamHandler
from aiologger.handlers.files import AsyncFileHandler
from aiologger.formatters.base import Formatter
from logging import LogRecord
from aiologger import Logger
import logging
import sys


class CustomFormatter(Formatter):
    def __init__(self, format: str):
        super().__init__(fmt=format)

    def format(self, record: LogRecord) -> str:
        return super().format(record)
    

async def setup_logger(handler_types: list = []) -> Logger:
    logger = Logger(name="./logs/log_file", level=logging.DEBUG)
    formatter = CustomFormatter("%(asctime)s - %(levelname)s - %(name)s - %(message)s")

    if "console" in handler_types:
        console_handler = AsyncStreamHandler(stream=sys.stdout)
        console_handler.formatter = formatter
        console_handler.level = logging.INFO
        logger.add_handler(console_handler)
    if "file" in handler_types:
        file_handler = AsyncFileHandler(filename="./logs/log_file.log")
        file_handler.formatter = formatter
        file_handler.level = logging.DEBUG
        logger.add_handler(file_handler)

    return logger