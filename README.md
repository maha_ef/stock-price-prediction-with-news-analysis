# Stock Price Prediction with News Analysis



## Step 0: Set environment
```python 
python3.11 -m venv myprojectenv  
source myprojectenv/bin/activate  
pip install --upgrade pip  
pip install -r requirements.txt
```

**Configs:**
* Fill configs/db/example.yaml with your credentials and tables' names and specify it's name in configs/config.yaml

## Step 1: Parse and load to database *(sync and async)*
```mermaid
graph LR
    Ent1[Main]
    Ent2[RBC parser]
    Ent3[DataBase]
    Ent4[Configs]
    Ent5[Parser config]
    Ent6[Logger config]
    Ent7[DataBase config]

    Ent5-->Ent4
    Ent6-->Ent4
    Ent7-->Ent4

    Ent4-->Ent1
    Ent1--"config, logger"-->Ent2
    Ent2--"data"-->Ent1
    
    Ent1--"config, logger, data"-->Ent3

```

**Configs**  
Using Hydra framework *(for sync only)*
* DataBase config: credentials for connection and info about table
* RBC parser config: info for parsing https://www.rbc.ru/
* Logger:
    * Logger path config: path to config (Hydra can't handle python objects in yaml)
    * Logger config: all info to create logger
* Main config: info about configs' hierarchy  

**parsers**
* rbc_parser
    * Parses data from RBC for a specified time period
    * Returns dataframe

**database_function**
* Implemets functions for interaction with database

**other**
* Implements utils functions

**main.py**
* Loads configs
* Creates logger
* Parses data sources and loads to the database

## Step 2: Connect news and price changes
```mermaid
graph 
    Ent1[Main]
    Ent2[MOEX parser]
    Ent3[DataBase]
    Ent4[Configs]
    Ent5[MOEX config]
    Ent6[Logger config]
    Ent7[DataBase config]
    Ent8[Main]
    Ent9[DataBase]

    Ent5-->Ent4
    Ent6-->Ent4
    Ent7-->Ent4

    Ent4-->Ent1
    Ent1--"config, logger"-->Ent2
    Ent2--"prices"-->Ent8

    Ent1--"config, logger"-->Ent3
    Ent3--"news data"-->Ent8
    Ent8--"grouped news with prices"-->Ent9
```
**Configs**  
Using Hydra framework
* MOEX config: dates to parse and stock's ticket

**parsers**
* moex_parser
    * Parses prices from MOEX for a specified time period
    * Returns dataframe with dates and price changes

**main.py**
* Loads configs
* Creates logger
* Parses moex 
* Fetches news from database
* Groups news by days and merges with price changes
* Loads table to database

