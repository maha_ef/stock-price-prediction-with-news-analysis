# from logging.config import dictConfig
from typing import List, Dict
from omegaconf import DictConfig
from logging import Logger
import pandas as pd
import datetime

def reduce_list(lst: List = None) -> List:
    """
    Reduce list of iterators with one element to usual list
    """
    return [t[0] for t in lst]


def filter_data_by_dates(
    data: pd.DataFrame = None,
    date_ranges: Dict = None
) -> pd.DataFrame:
    """
    Filters DataFrame by dates: keeps only rows older than min date or newer than max date
    """
    def filter_row(row: pd.Series = None) -> pd.Series:
        """
        Filters row by dates: keeps only rows older than min date or newer than max date
        """
        min_date, max_date = date_ranges.get(row["source"], [pd.Timestamp.max, pd.Timestamp.min])
        return row["publish_date"] < min_date or row["publish_date"] > max_date

    filtered_df = data[data.apply(filter_row, axis=1)]
    return filtered_df


def get_unique_date_intervals(
    new_date_left: pd.Timestamp = None,
    new_date_right: pd.Timestamp = None,
    existed_date_left: pd.Timestamp = None,
    existed_date_right: pd.Timestamp = None
) -> List:
    """
    Returns unique date intervals
    """
    if isinstance(new_date_left, datetime.date):
        new_date_left = new_date_left.strftime("%Y-%m-%d")
        new_date_right = new_date_right.strftime("%Y-%m-%d")
    new_date_left = datetime.datetime.strptime(new_date_left, "%Y-%m-%d")
    new_date_right = datetime.datetime.strptime(new_date_right, "%Y-%m-%d")

    if isinstance(new_date_left, datetime.date):
        existed_date_left = existed_date_left.strftime("%Y-%m-%d")
        existed_date_right = existed_date_right.strftime("%Y-%m-%d")
    existed_date_left = datetime.datetime.strptime(existed_date_left, "%Y-%m-%d")
    existed_date_right = datetime.datetime.strptime(existed_date_right, "%Y-%m-%d")

    unique_date_intervals = []
    
    if (new_date_right < existed_date_left) or (new_date_left > existed_date_right):
        unique_date_intervals.append([new_date_left, new_date_right])
    elif (new_date_left < existed_date_left) and (new_date_right <= existed_date_right):
        unique_date_intervals.append([new_date_left, existed_date_left])
    elif (new_date_right > existed_date_right) and (new_date_left >= existed_date_left):
        unique_date_intervals.append([existed_date_right, new_date_right])
    elif (new_date_left < existed_date_left) and (new_date_right > existed_date_right):
        unique_date_intervals.append([new_date_left, existed_date_left])
        unique_date_intervals.append([existed_date_right, new_date_right])
    
    for i in range(len(unique_date_intervals)):
        unique_date_intervals[i][0] = unique_date_intervals[i][0].strftime("%Y-%m-%d")
        unique_date_intervals[i][1] = unique_date_intervals[i][1].strftime("%Y-%m-%d")

    return unique_date_intervals


def adjust_dates(
    existed_dates: Dict,
    cfg: DictConfig,
    logger: Logger
) -> DictConfig: 
    """
    Adjusts dates:
        for left date takes config date, if it greater than min date in news table, else takes min date
        for right date takes config date, if it less than max date in news table, else takes max date

        Min date from news table is next day, if earliest new was before 12 AM, else the day next tommorrow
        Max date from news table is same day, if latest new was not earlier than at 9 AM, else previous day
    """
    min_news_date = min(start for start, _ in existed_dates.values())
    if min_news_date.time() <= datetime.datetime.strptime("12:00", "%H:%M").time():
        min_news_date += datetime.timedelta(days=1)
    else:
        min_news_date += datetime.timedelta(days=2)
    min_news_date = min_news_date.replace(hour=0, minute=0, second=0)

    max_news_date = max(end for _, end in existed_dates.values())
    if max_news_date.time() < datetime.datetime.strptime("09:30", "%H:%M").time():
        max_news_date -= datetime.timedelta(days=1)
    max_news_date = max_news_date.replace(hour=0, minute=0, second=0)

    config_date_from = datetime.datetime.strptime(cfg.experiment.dateFrom, "%Y-%m-%d")
    config_date_to = datetime.datetime.strptime(cfg.experiment.dateTo, "%Y-%m-%d")
    
    if config_date_from < min_news_date:
        logger.debug(f"Adjusting dateFrom in config to {min_news_date}")
        cfg.experiment.dateFrom = min_news_date.strftime("%Y-%m-%d")
    if config_date_to > max_news_date:
        logger.debug(f"Adjusting dateTo in config to {max_news_date}")
        cfg.experiment.dateTo = max_news_date.strftime("%Y-%m-%d")
    
    return cfg
    

def assign_date_group(row: pd.Series):
    if row.hour < 10 or (row.hour == 10 and row.minute < 30):
        return row.replace(hour=0, minute=0, second=0)
    else:
        return (row + datetime.timedelta(days=1))\
            .replace(hour=0, minute=0, second=0)


def group_weekends(
        df: pd.DataFrame,
        news_aggregation_type: str = "concat"
) -> pd.DataFrame:
    """
        If news_aggregation_type:
            1. 'concat' then concatenate all news withing a group
            2. 'share' then share price delta within a group
    """
    df["group"] = (df["price_change"] != 0).cumsum()
    df["as_previous"] = (df["group"] == df["group"].shift()).astype(int)
    df["group"] = df["group"] + df["as_previous"]
    if news_aggregation_type == "concat":
        df = df.groupby("group").agg(
            {
                "report_date": "last",
                "news": " ".join,
                "price_change": "last"
            }
        ).reset_index(drop=True)
    elif news_aggregation_type == "share":
        dates_and_prices = df[["group", "report_date", "price_change"]]
        dates_and_prices = dates_and_prices.groupby("group").agg(
            {
                "report_date": "last",
                "price_change": "last"
            }
        ).reset_index()
        news = df[["group", "news"]]
        df = pd.merge(
            left=news,
            right=dates_and_prices,
            how="inner",
            on="group"
        )
        df = df[["report_date", "news", "price_change"]]
    return df
